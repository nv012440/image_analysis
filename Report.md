# Image Analysis
```
Module Code: CS3IA16
Assignment Report Title: Image Analysis Group Coursework
Student Number: 27016005,27012440,2701979
Date: 29/10/2020
Actual hrs spent for the assignment:
Assignment evaluation:
```
## Effort Allocation
| Name              |Student Number |Contribution  |Note   |
| -------------     |-------------  |:-----------: |-------|
| Daniel Broomhead  | 27016005      | 100          |       |
| Edwin Park        | 27012440      | 100          |       |
| Savva Podkopov    | 27017979      | 090          |       |


This project aims to illustrate the basic techniques used in image analysis 
for image enhancement in both spatial and frequency domains. 
Free-Fourier Transform was applied, and then frequency filters were 
applied in order to negate the periodic noise in the frequency domain, 
while various filters were applied to nullify the random noise. 
once all of the noise has been removed, to the fullest extent - the manipulated 
distorted image can then be compared to the original undistorted image by using the Measure of Mean Square Error (MSE). 


## Introduction
The Fundamental principles of image enhancement revolves around the objective of making an image more suitable to a specific application once it has been processed.  
The purpose of this process was in order to prove that it is possible to enhance a distorted image to a usable quality through the process of using various filters, transformations and manipulations in both the spatial and frequency domains. 


## Methodology

### Design

Where Periodic noise has been affected marked in **BOLD**

Where Random Noise has been affected marked in  *ITALIC*






 
Process: 


* First a median filter was applied. A median filter is a “non-linear digital filtering technique” used to remove random noise from an image. It completes this by replacing each signal with the median value of all the surrounding frequencies. This is a preprocessing technique predominantly used before any of the alternate processes, due to the ability of preserving edges and finer details while also removing noise. [1].*

* MedFilt2(Image): A 2-Dimensional Median filter was used. Meaning the median value was taken over a 3x3 Grid around each pixel. The option of using another domain region was considered, however it did not seem to be much more effective and was deemed unnecessary. [2]*



**Next a two-dimensional FFT algorithm was applied using the inbuilt function which comes as part of the standard MATLAB package.**

**Y = fft2(X);**

**Which is the equivalent of fft(fft(X).’).’ [3]**


**Next an FFT Shift was performed by using another inbuilt MATLAB function: **

**Y = fftshift(X);**

The objective of the FFT Shift transforms the FFT through shifting the zero-frequency component to the centre of the array. [4]


After this, the “abs()” function was used to return the absolute value of each respective signal, the FFT produces an output which contains elements of both complex and real nature, by using the “abs” function, we can obtain the complex magnitude of these results. [5]

Next the Natural logarithm was applied using the “log(X)” function, which actually performs ln(X), This entire process up to this point removes the undesirable background “static-esque” noise. 

A variable was created called “ampThreshold”, this was then used as a filter, to convert the image into a binary format, representing whether the intensity (amplitude) of the signal exceeded the threshold given (0 for threshold not met, 1 for threshold met/exceeded ). After extensive testing including a manual binary search, a value of 8.5 was deemed to be most effective. 

A mask is a method of preserving a specified domain of frequencies which contain desirable data and attempting to exclude non-desirable information. Due to the FFT shift, it was apparent that all the desirable information would lie at the centre of the manipulated image, this could be easily proved through inspection. There lies a clear domain at the centre of the image which consists of masses of relevant information, which needed preserving. 


A mask was set up, on the black-and-white model, from (x =120, y = 205) to (x= 250, y = 455) in a rectangular formation, which set the intensity of each cell to 0. This image is then applied as a mask to the original FFT image created at the start. 
(Image dimensions: x = 371, y = 660) – (image processed where x= height, y= width)  

The log() and abs() then have to be reapplied to this image, to show the current state in a representable visual form before a reverse reconstruction can be attempted on the image version prior to the log() and abs() manipulations. 

From here the reverse reconstruction can be initiated, this starts with another fftshift() with the purpose of recentre-ing the zero-frequency. 

After this an ifft2(x) function is used to compute the inverse discrete Fourier Transform via a fast fourier transform algorithm, in two dimensions.  This is then again altered via the abs() function. 

After which the complex array can be converted into an integer array using the uint8() function, which is an inbuilt function which converts each value to an 8-bit unsigned integer format (double precision). 
[6]
[7]

From here minor aesthetic changes can be made in an attempt to get the restored image closer to what it should look like (as shown in the undistorted reference image).

One of the alterations made was a contrast adjustment using the imadjust() function built into MATLAB. 

    **





*After this was done, some random “salt and pepper” noise was still observed, therefore the image was passed through another two-dimensional median filter, to create the final result. 
*


A calculation was then run using the immse() function of MATLAB to determine the Mean-Square Error, this calculation was run on the original distorted image, with respect to the undistorted reference image, as well as the final enhanced version, also against the undistorted reference image. 
These two values were used in tandem to compare the overall enhancement and improvement from the original distorted image to the restored version - with respect to the reference image.   
 


#### Periodic Noise


 To remove periodic noise, a Fast Fourier Transform (FFT) function will be applied to the image. A Fast Fourier Transformation is a variant on the Discrete Fourier transform (DFT)  which uses a  method of mapping the time/space domain onto the frequency domain in an attempt to make it more representable and visualisable. The Fast Fourier variant is a much quicker, less extensive form of Discrete Fourier Transformation. 
This means that the signals within the image can be seen, displayed and manipulated more easily. 



The signals in this case would be the grayscale intensity of each pixel.
The image will also be subject to an FFTShift which displaces the zero-frequency component to the centre.


This will ensure that the main set of frequencies (the image we want) will be centred.
After performing the FFT, a logarithm will be applied to the array of frequencies, which highlights the main frequencies that represent the image.


As the main pattern is known to be central, a mask can be generated to preserve it while removing the unwanted spikes (the periodic noise).


After the mask has been made, it can be applied to the original FFT to negate everything apart from the wanted components.

The filtered FFT map can then be revered using another FFT shift and an inverse FFT function to recreate the image.

#### Random Noise

To remove the random noise, a median filter will be applied to the image.
This will work to remove the random noise as it will filter out any outliers in the spatial domain.
As most, if not all, the random noise appears as white speckles on the image,
they should be filtered out due to the large difference in intensity compared to the rest of the image

### Implementation

The design has been implemented in MATLAB.
The main reason for this choice was as Matlab comes pre-installed with a large bank of libraries that aid image manipulation and enhancement.
This means that the setup was very simple as only Matlab needs to be installed for the project implementation.

## Results and Discussion

#### Implementing Mean Square Error(MSE)

The MSE was easily implemented as Matlab has an MSE function prebuilt.
The function takes two parameters; the initial image and the second image for comparison.
The code for the MSE calculation is as follows:
```
err1 = immse(imgOriginal, imgNoise);
```
The first parameter is the original undistorted image and then the second image is the image to be compared.
For comparison, we calculated the MSE of the initial distorted image and then the MSE of the enhanced image.

### Reviewing Results of MSE

Ideally the MSE would be 0, meaning that there is no difference between the original and enhanced image and that they are identical,
However this is not realistic so the aim is to achieve a comparably low value to the starting MSE.
The MSE of the distorted image was 1575.522396 and the MSE of the final enhanced image was 81.546786.
Using the calculation 100 - 100(81.../1575...) the improvement can be calculated.
The result is a 94.824143% increase in accuracy of the image.

## Conclusion

The results of this project were very high as the image was recreated with a high degree of accuracy.
Upon inspection by eye the image is mostly the same, with some noticeable differences,
such as:
* Some minor noise still present around the edges;
* The blacks and whites of the image are not as distinct
* The background is more blurry

Despite these minor issues the image is mostly similar to the original.
This statement further supported the MSE which is vastly lower than the starting distorted image.

For the enhancement a mask was used on the FFT,
but the group also experimented with designing filters to remove the periodic noise.
Despite this attempt at creating filters, the group could not implement them successfully.
If this project could be repeated, the main focus would be to implement filters as they would be more versatile than a mask,
due to the nature of how each method operates.
A mask has to be adjusted to suit each image as they are unique whereas a filter can be designed to automatically pinpoint the undesirable frequencies and remove them.



```
#### Figures
Figure 1 - First Median Filter:

![Figure 1](https://csgitlab.reading.ac.uk/nv012440/image_analysis/-/raw/master/images/screenshots/MedianFilter.jpg)

Figure 2 - FFT:

![Figure 2](https://csgitlab.reading.ac.uk/nv012440/image_analysis/-/raw/master/images/screenshots/FFT.jpg)

Figure 3 - Log and Absolute of FFT:

![Figure 3](https://csgitlab.reading.ac.uk/nv012440/image_analysis/-/raw/master/images/screenshots/LogFFT.jpg)

Figure 4 - AmpThreshold:

![Figure 4](https://csgitlab.reading.ac.uk/nv012440/image_analysis/-/raw/master/images/screenshots/Threshold.jpg)

Figure 5 - Creating the Mask:

![Figure 5](https://csgitlab.reading.ac.uk/nv012440/image_analysis/-/raw/master/images/screenshots/Mask.jpg)

Figure 6 - Applying the Mask:

![Figure 6](https://csgitlab.reading.ac.uk/nv012440/image_analysis/-/raw/master/images/screenshots/MaskApplied.jpg)

Figure 7 - Recreating the image after Mask is applied:

![Figure 7](https://csgitlab.reading.ac.uk/nv012440/image_analysis/-/raw/master/images/screenshots/NoNoise.jpg)

Figure 8 - Adjusting contrast of the image:

![Figure 8](https://csgitlab.reading.ac.uk/nv012440/image_analysis/-/raw/master/images/screenshots/Contrast.jpg)

Figure 9 - Applying second median filter:

![Figure 9](https://csgitlab.reading.ac.uk/nv012440/image_analysis/-/raw/master/images/screenshots/Final.jpg)














#### References
[1]En.wikipedia.org. 2020. Median Filter. [online] Available at: <https://en.wikipedia.org/wiki/Median_filter#:~:text=The%20median%20filter%20is%20a,edge%20detection%20on%20an%20image)> [Accessed 29 October 2020].
(Median filter, 2020)

[2]Mathworks.com. 2020. 2-D Median Filtering - MATLAB Medfilt2. [online] Available at: <https://www.mathworks.com/help/images/ref/medfilt2.html> [Accessed 29 October 2020].
(2-D median filtering - MATLAB medfilt2, 2020)

[3]Uk.mathworks.com. 2020. 2-D Fast Fourier Transform - MATLAB Fft2- Mathworks United Kingdom. [online] Available at: <https://uk.mathworks.com/help/matlab/ref/fft2.html?s_tid=srchtitle> [Accessed 29 October 2020].
(2-D fast Fourier transform - MATLAB fft2- MathWorks United Kingdom, 2020)

[4]Uk.mathworks.com. 2020. Shift Zero-Frequency Component To Center Of Spectrum - MATLAB Fftshift- Mathworks United Kingdom. [online] Available at: <https://uk.mathworks.com/help/matlab/ref/fftshift.html?s_tid=srchtitle> [Accessed 29 October 2020].
(Shift zero-frequency component to center of spectrum - MATLAB fftshift- MathWorks United Kingdom, 2020)

[5]Uk.mathworks.com. 2020. Absolute Value And Complex Magnitude - MATLAB Abs- Mathworks United Kingdom. [online] Available at: <https://uk.mathworks.com/help/matlab/ref/abs.html> [Accessed 29 October 2020].
(Absolute value and complex magnitude - MATLAB abs- MathWorks United Kingdom, 2020)

[6]Uk.mathworks.com. 2020. 8-Bit Unsigned Integer Arrays - MATLAB- Mathworks United Kingdom. [online] Available at: <https://uk.mathworks.com/help/matlab/ref/uint8.html> [Accessed 29 October 2020].
(8-bit unsigned integer arrays - MATLAB- MathWorks United Kingdom, 2020)

[7]Uk.mathworks.com. 2020. 2-D Inverse Fast Fourier Transform - MATLAB Ifft2- Mathworks United Kingdom. [online] Available at: <https://uk.mathworks.com/help/matlab/ref/ifft2.html?s_tid=srchtitle> [Accessed 29 October 2020].
(2-D inverse fast Fourier transform - MATLAB ifft2- MathWorks United Kingdom, 2020)

 

################################################################
mEDIAN FILTER  - salt,pepper
FFT 
FFT shift → moves zero frequency to centre
Absol - complex part removal 
Log
White static removed
Amp threshold 
Create mask 
	Manual 
	Automatic
		Takes dimensions →  ⅓ / ⅓ 
Apply mask to original fft (post absolute log)

IFFT 
IFFT shift
Recreates an image

Contrast adjustments
Median filter
MSE 
Percentage
 


